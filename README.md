Fusionary Capistrano Tasks
==========================

Fusionary's own capistrano tasks

Installation
============

Add to your Gemfile:

```ruby
    gem 'fusionary-capistrano-tasks',
      :require => nil,
      :git => 'git@bitbucket.org:fusionary/capistrano-recipes.git'
```


Add to config/deploy.rb:

```ruby
    require 'fusionary-capistrano-tasks/php' # For php apps
    require 'fusionary-capistrano-tasks/rails' # For Rails apps
    require 'fusionary-capistrano-tasks/deploy/passenger' # For Phusion Passenger deployment
```

Changes
=======

Instead of using env variables we're going to do it the Capistrano Way and pass them in via the command line.

For example:

    STAGE=live cap deploy

becomes:

    cap -S stage=live deploy

Examples
========

    cap -S stage=live deploy
    cap -S branch=dev deploy

Also see:

    Gemfile.sample
    deploy.rb.php.sample
    deploy.rb.sample



Copyright (c) 2013 Fusionary Media, Inc.
