Capistrano::Configuration.instance.load do
  namespace :deploy do
    desc 'Restarting passenger with restart.txt'
    task :restart, :roles => :app do
      run "touch #{current_path}/tmp/restart.txt"
    end

    [:start, :stop].each do |t|
      desc "#{t} task is a no-op with passenger"
      task t, :roles => :app do ; end
    end
  end
end
