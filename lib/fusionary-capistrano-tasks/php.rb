Capistrano::Configuration.instance.load do
  require File.dirname(__FILE__) + '/util'

  # Default values
  set :keep_releases,         3
  set :app_symlinks,          nil
  set :extension_symlinks,    nil
  set :shared_dirs,           nil
  set :extra_permissions,     nil
  set :database_yml_path,     'config/database.yml'
  set(:composer_copy_vendors, true) unless exists?(:composer_copy_vendors)

  # Callbacks
  before 'deploy',                  'util:capture_pending_changes'
  before 'deploy',                  'util:verify_deploy'
  after  'deploy',                  'deploy:cleanup'
  after  'deploy',                  'util:notify'
  after  'deploy',                  'util:notify_tracker'
  before 'deploy:setup',            'util:verify_deploy'
  after  'deploy:setup',            'fusionary:setup_shared'
  after  'deploy:create_symlink',   'fusionary:symlink_extras'
  before 'deploy:rollback',         'util:verify_deploy'
  before 'deploy:pending',          'util:verify_deploy'
  before 'util:remove_cached_copy', 'util:verify_deploy'
  after  'fusionary:setup_shared',  'fusionary:set_extra_permissions'

  namespace :deploy do
    task :restart do
      puts 'This is a no-op in PHP'
    end
  end

  namespace :fusionary do
    desc 'Setup additional symlinks to shared directories'
    task :symlink_extras, :roles => [:web] do
      if app_symlinks
        app_symlinks.each { |link| run "ln -nfs #{shared_path}/#{link} #{current_path}/#{link}" }
      end
    end

    desc 'Setup the additional shared directories'
    task :setup_shared, :roles => [:web] do
      if shared_dirs
        shared_dirs.each do |dir|
          run "mkdir -p #{shared_path}/#{dir}"
        end
      end
    end

    desc 'Setup the additional shared directories (locally)'
    task :setup_local_shared do
      if shared_dirs
        shared_dirs.each do |dir|
          `mkdir -p #{dir}`
        end
      end
    end

    desc 'Set permissions on directories'
    task :set_extra_permissions, :roles => [:web] do
      if extra_permissions
        extra_permissions.each do |dir, permissions|
          run "if [ -e #{shared_path}/#{dir} ]; then chmod -R #{permissions} #{shared_path}/#{dir}; fi"
        end
      end
    end

    desc 'Set permissions on directories (locally)'
    task :set_local_extra_permissions do
      if extra_permissions
        extra_permissions.each do |dir, permissions|
          if File.exist?("#{dir}")
            `chmod -R #{permissions} #{dir}`
          end
        end
      end
    end

    desc "Run local setup (create directories and set permissions)"
    task :local_setup do
      setup_local_shared
      set_local_extra_permissions
    end
  end # fusionary

  namespace :composer do

    # adapted this from https://gist.github.com/jmather/3715541
    desc "Copy vendors from previous release"
    task :copy_vendors, :except => { :no_release => true } do
      composer_json_path = "#{latest_release}/composer.json"
      composer_vendor_dir = "vendor"
      if remote_file_exists?(composer_json_path)
        run "cat #{composer_json_path}" do |ch, st, data|
          json = JSON.parse(data.strip)
          if json && json['config'] && json['config']['vendor-dir']
            composer_vendor_dir = json['config']['vendor-dir']
          end
        end
      end

      composer_vendor_dir = Pathname.new(composer_vendor_dir).cleanpath

      if remote_file_exists?("#{previous_release}/#{composer_vendor_dir}")
        run "if [ -d #{previous_release}/#{composer_vendor_dir} ]; then mkdir -p #{latest_release}/#{composer_vendor_dir} && rsync -az #{previous_release}/#{composer_vendor_dir}/ #{latest_release}/#{composer_vendor_dir}/; fi"
      else
        puts "Previous Composer vendors not found, falling back to install."
        composer.install
      end
    end

    # adapted this from https://github.com/everzet/capifony/blob/master/lib/symfony2/symfony.rb
    desc "Gets composer and installs it"
    task :get do
      if !remote_file_exists?("#{latest_release}/composer.phar")
        puts 'Downloading Composer'.green
        run "sh -c 'cd #{latest_release} && curl -s http://getcomposer.org/installer | php'"
      else
        puts 'Updating Composer'.green
        run "sh -c 'cd #{latest_release} && php composer.phar self-update'"
      end
    end

    desc "run composer install and ensure all dependencies are installed"
    task :install, :roles => :app, :except => { :no_release => true } do
      if ( defined? composer_bin ) == nil
        composer.get
        set :composer_bin, "php composer.phar"
      end

      if !remote_file_exists?("#{latest_release}/composer.lock")
        puts 'Installing dependencies'
        run "sh -c 'cd #{latest_release} && #{composer_bin} install --no-dev'"
      else
        puts 'Updating dependencies'
        run "sh -c 'cd #{latest_release} && #{composer_bin} update --no-dev'"
      end
    end

  end

end
