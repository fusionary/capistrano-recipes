Capistrano::Configuration.instance.load do
  require File.dirname(__FILE__) + '/util'

  set :app_symlinks,    nil
  set :keep_releases,   3
  set :use_sudo,        false
  set :rails_env,       'development'

  # Callbacks
  before 'deploy',                'util:capture_pending_changes'
  after 'deploy',                 'deploy:cleanup'
  after 'deploy',                 'util:notify'
  after 'deploy',                 'util:notify_tracker'
  after 'deploy:update_code',     'fusionary:symlink_database_yml'
  after 'deploy:create_symlink',  'fusionary:symlink_extras'
  after 'deploy:setup',           'fusionary:create_shared_config'
  after 'deploy:setup',           'fusionary:setup_symlinks'

  namespace :fusionary do
    desc 'symlink database.yml from shared to release directory'
    task :symlink_database_yml, :roles => [:web, :app] do
      run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config"
    end

    desc 'create shared config directory'
    task :create_shared_config, :roles => [:app, :web] do
      run "mkdir -p #{shared_path}/config"
    end

    desc 'Setup additional symlinks for app'
    task :setup_symlinks, :roles => [:app, :web] do
      if app_symlinks
        app_symlinks.each do |link|
          if link.split('/').last.include? '.'
            run "mkdir -p #{shared_path}/#{File.dirname(link)}"
          else
            run "mkdir -p #{shared_path}/#{link}"
          end
        end
      end
    end

    task :symlink_extras, :roles => [:app, :web] do
      if app_symlinks
        app_symlinks.each { |link| run "ln -nfs #{shared_path}/#{link} #{current_path}/#{link}" }
      end
    end
  end
end
