## This is a sample deploy.rb script for a PHP application
## Edit it until it suits the needs of the application you're deploying

require 'fusionary-capistrano-tasks'
require 'fusionary-capistrano-tasks/php'

# Base options
set :user,        'shelluser'
set :application, 'website'
set(:stage, 'dev') unless exists?(:stage)
set :use_sudo, false

# This enables you to use the same SSH keys
# that you use for gh/bb on the remote (deploy to) host
ssh_options[:forward_agent] = true

# SCM options
set :repository,  'git@github.com:fusionary/path-to-repo'
set :scm,         'git'
set :git_shallow_clone, 1
set :deploy_via, :remote_cache
set :git_enable_submodules, 1
set :keep_releases, 3
set(:branch, 'master') unless exists?(:branch)

# You can also deploy from the current git branch.
# set :branch,      current_git_branch



# For DB Syncing
set :database_yml_path, 'config/database.yml'
set :remote_dump_path, "#{deploy_to}/current/tmp/#{stage}_dump.sql.gz"

# Notification options
set :deployment_tracker_url, 'http://tracker.fusionary.com/create'
set :deployment_tracker_api_key, 'xxxxxxxx'


# These get symlinked from /path/to/app/shared/
# into the current directory
set :app_symlinks, [
                    'config/config.env.php',
                    'system/expressionengine/cache',
                    'sites/website/public/files',
                    'sites/website/public/cache'
                   ]

# Change permissions on these directories
set :shared_dirs, [
                   'tmp/',
                   'config/',
                   'system/expressionengine/cache/',

                   # ee uploads
                   'sites/website/public/files/members/avatars/uploads/',
                   'sites/website/public/files/members/member-uploads/',
                   'sites/website/public/files/members/member-photos/',
                   'sites/website/public/files/members/signature-attachments/',
                   'sites/website/public/files/members/pm-attachments/',

                   # public cache
                   'sites/website/public/cache/',
                   'sites/website/public/cache/captcha/',
                   'sites/website/public/cache/made/',
                   'sites/website/public/cache/remote/',

                   # public files
                   'sites/website/public/files/docs/resources/',
                   'sites/website/public/files/forms/',
                   'sites/website/public/files/images/banners/',
                   'sites/website/public/files/images/features',
                   'sites/website/public/files/images/icons/',
                   'sites/website/public/files/images/data-centers/',
                   'sites/website/public/files/images/logos/',
                   'sites/website/public/files/images/misc/',
                   'sites/website/public/files/images/resources/',
                   'sites/website/public/files/images/people/',
                   'sites/website/public/files/videos/mp4/',
                   'sites/website/public/files/videos/webm/',
                   'sites/website/public/files/images/blog/'
                  ]

set :extra_permissions, {
  'sites/website/public/files/' => '777',
  'sites/website/public/cache/' => '777',
  'system/expressionengine/cache/' => '777'
}

# Set branches and servers
case stage
  # dev and live are on the same server
when 'live'
  server 'yourserver.com', :web, :app, :db
  set :server, 'yourserver.com'

when 'dev'
  server 'yourdevserver.com', :web, :app, :db
  set :server, 'yourdevserver.com'
else
  raise 'Error: Unkown deploy environment.'
end

set :deploy_to, "/home/#{user}/apps/#{application}/#{stage}"

# Local asset syncing
set :asset_dirs, [{
                    :server => fetch(:server),
                    :directory => "#{shared_path}/sites/website/public/files/*",
                    :local_directory => 'sites/website/public/files',
                    :exclude => ['cache/']
                  }]

# Before deploy
before(:deploy) do

  # Variables
  compile_compass = ENV['COMPASS'] || 'n'
  quantize_img    = ENV['OPTIMIZE_IMG'] || 'n'
  optimize_img    = ENV['QUANTIZE_IMG'] || 'n'
  local_commands  = []
  site_dirs       = [
                     'sites/website'
                    ]

  # For each site...
  site_dirs.each do |site|

    # Collect commands
    if compile_compass =~ /[Yy]/
      quantize_img = Capistrano::CLI.ui.ask "Quantize images for \"#{site}\"? (y/N)"
      optimize_img = Capistrano::CLI.ui.ask "Optimize images for \"#{site}\"? (y/N)"
      local_commands.push("QUANTIZE_IMG='#{quantize_img}' OPTIMIZE_IMG='#{optimize_img}' compass compile -e production --force #{site}")
      local_commands.push("git add #{site}/public/assets/styles/*")
      local_commands.push("git ci -a -m 'Compiling Compass stylesheets for \"#{site}\"' --quiet --no-status")
    end

  end

  # Run commands
  unless local_commands.empty?
    local_commands.push('git push')
    local_commands.each do |cmd|
      run_locally cmd
    end
  end

end

# After deploy
after(:deploy) do

  # Avoid EE CP errors by setting a few sourced files/dirs writable
  run 'chmod 666 #{release_path}/system/expressionengine/config/config.php'
  run 'chmod 666 #{release_path}/system/expressionengine/config/database.php'
  run 'chmod 777 #{release_path}/templates/'

  # Varnish
  # if stage == 'live'
  #   run '/usr/bin/varnishadm -T 127.0.0.1:6082 -S /etc/varnish/secret url.purge .'
  # end

end
