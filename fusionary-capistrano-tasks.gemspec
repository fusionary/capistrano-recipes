# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "fusionary-capistrano-tasks/version"

Gem::Specification.new do |s|
  s.name        = "fusionary-capistrano-tasks"
  s.version     = FusionaryCapistranoTasks::VERSION
  s.authors     = ["Jason Stewart", "Tim Kelty"]
  s.email       = ["jstewart@fusionary.com", "tkelty@fusionary.com"]
  s.homepage    = "https://bitbucket.org/fusionary/capistrano-recipes"
  s.summary     = "Fusionary's very own cap tasks"
  s.description = "Fusionary's very own cap tasks"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  s.add_development_dependency "capistrano", "> 2.0.0"
  s.add_runtime_dependency("colored", "~> 1.2")
  s.add_runtime_dependency("json", "~> 1.7.6")
end
